<?php 
class C_tambahdata extends CI_Controller{
	    function index(){
	    	$this->load->library('form_validation');
			$this->form_validation->set_rules('kategori','Kategori','required');
			$this->form_validation->set_rules('program','Program','required');
			$this->form_validation->set_rules('kegiatan','Kegiatan','required');
			$this->form_validation->set_rules('pptk','Kegiatan','required');

			if ($this->form_validation->run()==true) {
				$hasil="SELECT nama_program FROM program WHERE nama_program='".$this->input->post('program',true)."'";
				$id= $this->db->query($hasil);
				foreach ($id->result_array() as $get) {
					$nama_program = $get['nama_program'];
				}
				if ($nama_program==''){
					$data1 = array(
			   			'nama_program'			=> $this->input->post('program',true)
			   		);
					$this->db->insert('program',$data1); 
				}
			$hasil="SELECT id_program FROM program WHERE nama_program='".$this->input->post('program',true)."'";
			$id= $this->db->query($hasil);
			foreach ($id->result_array() as $get) {
				$id_program = $get['id_program'];
			}
	   		$data2 = array(
			   	'nama_kegiatan' 		=> $this->input->post('kegiatan',true),
			   	'id_bidang' 			=> $this->input->post('kategori',true),
			   	'id_pptk' 				=> $this->input->post('pptk',true),
			   	'id_program'			=> $id_program
			   	);	
	   		
			$this->db->insert('kegiatan',$data2);  
		   	}	
		   	redirect("C_Administrator");
		   	


		}
		function edit(){
	    	$this->load->library('form_validation');
			$this->form_validation->set_rules('kategori','Kategori','required');
			$this->form_validation->set_rules('program','Program','required');
			$this->form_validation->set_rules('kegiatan','Kegiatan','required');
			$this->form_validation->set_rules('pptk','Kegiatan','required');

			if ($this->form_validation->run()==true) {
			$data1 = array(
			   	'nama_program'			=> $this->input->post('program',true)
			   	);
			$this->db->where('id_program', $this->input->post('id_program',true));
            $this->db->update('program', $data1);
	   		$data2 = array(
			   	'nama_kegiatan' 		=> $this->input->post('kegiatan',true),
			   	'id_bidang' 			=> $this->input->post('kategori',true),
			   	'id_pptk' 				=> $this->input->post('pptk',true),
			   	'id_program'			=> $this->input->post('id_program',true)
			   	);	
	   		
			$this->db->where('id_kegiatan', $this->input->post('id_kegiatan',true));
            $this->db->update('kegiatan', $data2); 
		   	}	
		   	redirect("C_Administrator/databelanja");
		}
		function edit_user(){
	    	$this->load->library('form_validation');
			$this->form_validation->set_rules('username1','Username','required');
			$this->form_validation->set_rules('pass1','Password','required');

			if ($this->form_validation->run()==true) {
			$data1 = array(
			   	'username'			=> $this->input->post('username1',true),
			   	'password'			=> $this->input->post('pass1',true)
			   	);
			$this->db->where('id_pengguna', $this->input->post('idpengguna',true));
            $this->db->update('pengguna', $data1);
	   		 
		   	}	
		   	redirect("C_Administrator/datauser");
		}	

		function editbidang(){
	    	$data1 = array(
			   	'pagu' 		=> $this->input->post('pagu',true)
			   	);	
	   		
			$this->db->where('id_kegiatan', $this->input->post('id_kegiatan',true));
            $this->db->update('kegiatan', $data1);
	   		$data2 = array(
			   	'tri1' 		=> $this->input->post('kas1',true),
			   	'tri2' 		=> $this->input->post('kas2',true),
			   	'tri3' 		=> $this->input->post('kas3',true),
			   	'tri4' 		=> $this->input->post('kas4',true),
			   	'realkeu' 	=> $this->input->post('realk',true),
			   	'realfisik'	=> $this->input->post('realfisik',true)
			   	);	
	   		
			$this->db->where('id_kegiatan', $this->input->post('id_kegiatan',true));
            $this->db->update('kegiatan', $data2); 
		   		
		   	redirect("C_Administrator/bidang");
		}	
}

?>