<?php
class C_login extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('M_login');
	}
	public function aksi_login()
	{
		$un=$this->input->post('username');
		$pass=$this->input->post('password');
		$us=$this->input->post('user');


		$this->load->library('form_validation');

		$this->form_validation->set_rules('username','Username','required');
		$this->form_validation->set_rules('password','Password','required');
		$this->form_validation->set_rules('user','Bidang','required');

		if ($this->form_validation->run()==true) {
	   		$where = array(
			   	'username' 				=> $un,
			   	'password' 				=> $pass,
			   	'nama_bidang'			=> $us
			   	);		
	   	}	
	   	else {
	   		redirect('');
	   	}

		$cek = $this->M_login->cek_login('pengguna',$where)->num_rows();
		if($cek > 0){
			$hasil="SELECT lvl_user FROM pengguna WHERE nama_bidang='".$us."'";
			$lvl= $this->db->query($hasil);
			foreach ($lvl->result_array() as $level) {
				$lv = $level['lvl_user'];
			}
			 // echo $us;
			$data_session = array(
				'username' => $un, 
				'user' => $us
			);

			$this->session->set_userdata($data_session);


			if ($lv==1){
				redirect('C_Administrator');
			}
			elseif ($lv==2){

				redirect('C_Administrator/bidang');
			}
			else{
				redirect('');
			}
		   	

	  	}else{
	  		echo "<script>alert('Username dan Password Salah!');history.go(-1);</script>";
	   		//echo "Username dan Password salah!";
	   		//redirect('');
	  	}

	}
}