<?php
	class C_hapus extends CI_Controller{
		function __construct(){
			  parent::__construct();
			  $this->load->model('M_hapus');
		 }
		 public function hapusdata($id_kegiatan){
		 	$where = array('id_kegiatan' => $id_kegiatan);
		 	$this->M_hapus->hapus($where,'kegiatan');
		 	redirect('C_Administrator/databelanja');
		 }

		 public function hapusdatabdg($id_kegiatan){
		 	$data['id'] = $this->M_hapus->hapus_data_bdg($id_kegiatan);
		 	redirect('C_Administrator/bidang');
		 }	 
	}
?>