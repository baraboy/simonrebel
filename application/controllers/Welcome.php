<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct(){
	        parent::__construct();
	        $this->load->model('M_datatabel');
	    }
	public function index()
	{

		$x['info'] = $this->db->query("SELECT * FROM kegiatan JOIN bidang USING(id_bidang)")->result_array();
		$x['sum'] = $this->db->query("SELECT id_bidang, sum(pagu) as Total FROM kegiatan GROUP BY id_bidang")->result_array();
		$x['max'] = $this->db->query("SELECT MAX(id_bidang) as max FROM kegiatan")->row_array();

		$i = 1;
		foreach ($x['sum'] as $key) {
			if($i!=null){
				$total[$i] = $key['Total'];
			}
			else{
				continue;
			}

			$i++;
		}
		$x['angka'] = $total;
		$x['data']=$this->M_datatabel->tabel1();
		$x['data2']=$this->M_datatabel->tabel2();
		$x['data3']=$this->M_datatabel->chart1();
		$this->load->view('welcome_message',$x);
	}


}
