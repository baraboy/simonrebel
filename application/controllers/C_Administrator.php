<?php
	class C_Administrator extends CI_Controller{
	    function __construct(){
	        parent::__construct();
	        $this->load->model('M_login');
	        $this->load->model('M_datatabel');
	        $this->load->library('excel');
	    }
	    function index(){
	    	if($this->session->userdata('user')==('Administrator')){	
		 		$x['data']=$this->M_login->get_kategori();
       	 		$this->load->view('admin/admin',$x);
			}
			else{
				redirect('');
			}
		}	
		function get_subkategori(){
	        $id=$this->input->post('id');
	        $data=$this->M_login->get_subkategori($id);
	        echo json_encode($data);
	    }	
	    function bidang(){	
	    	if($this->session->userdata('user')!=('')){
	    		$x['databdg']=$this->M_datatabel->get_data_bidang($this->session->userdata('user'));
       	 		$this->load->view('bidang/databidang',$x);
       	 	}
       	 	else{
       	 		redirect('');
       	 	}
		}
		function printbdg(){
		  $object = new PHPExcel();

		  $object->setActiveSheetIndex(0);

		  $table_columns = array("Nama Kegiatan", "Nama Program", "Nama PPTK", "Pagu/Anggaran", "Rencana Kas Triwulan 1", "Rencana Kas Triwulan 2", "Rencana Kas Triwulan 3", "Rencana Kas Triwulan 4", "Realisasi Keuangan", "Realisasi Fisik");

		  $column = 0;

		  foreach($table_columns as $field)
		  {
		   $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
		   $column++;
		  }

		  $akun = $this->session->userdata('user');
		  $databpkad = $this->M_datatabel->get_data_bidang($akun);

		  $excel_row = 2;

		  foreach($databpkad as $row)
		  {
		   $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->nama_kegiatan);
		   $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->nama_program);
		   $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->nama_pptk);
		   $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->pagu);
		   $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->tri1);
		   $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->tri2);
		   $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->tri3);
		   $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->tri4);
		   $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row->realkeu);
		   $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row->realfisik);

		   $excel_row++;
		  }

		  $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		  header('Content-Type: application/vnd.ms-excel');
		  header('Content-Disposition: attachment;filename="Data Belanja Bidang '.$akun.'.xls"');
		  $object_writer->save('php://output');
		}

		function printadmin(){
		  $object = new PHPExcel();

		  $object->setActiveSheetIndex(0);

		  $table_columns = array("Nama Kegiatan", "Nama Program", "Bidang", "Nama PPTK" , "Jabatan PPTK");

		  $column = 0;

		  foreach($table_columns as $field)
		  {
		   $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
		   $column++;
		  }

		  $dataadminbpkad = $this->M_datatabel->get_data();

		  $excel_row = 2;

		  foreach($dataadminbpkad as $row)
		  {
		   $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->nama_kegiatan);
		   $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->nama_program);
		   $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->nama_bidang);
		   $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->nama_pptk);
		   $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->jabatan);
		   $excel_row++;
		  }

		  $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		  header('Content-Type: application/vnd.ms-excel');
		  header('Content-Disposition: attachment;filename="Data Administrator BPKAD.xls"');
		  $object_writer->save('php://output');
		 }

		function logout(){
			$this->session->sess_destroy();
			redirect('');
		}
		function databelanja(){
			$x['datax']=$this->M_datatabel->get_data();
			$this->load->view('admin/databelanja',$x);
		}
		function datauser(){
			$y['dataus']=$this->M_datatabel->get_data_user();
			$this->load->view('admin/datauser',$y);
		}
		function coba(){
			$id = $_POST['id_kegiatan'];
			$no = $_POST['no'];
		 	$sql = "SELECT * FROM bidang";
          	$query = $this->db->query($sql);
          	echo "
        
                  		<option value='0'>-PILIH-</option>
          	";
            foreach ($query->result_array() as $row) {
                echo "<option value='".$row['id_bidang']."'>".$row['nama_bidang']."</option>";
            }
		}	
	}
