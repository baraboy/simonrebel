<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>SIMONREBEL BPKAD NTB</title>
  <meta name="description" content="Free Bootstrap Theme by BootstrapMade.com">
  <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">
  <link rel="shortcut icon" href="assets/img/logontb.jpg">
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans|Candal|Alegreya+Sans">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/font-awesome.min.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/imagehover.min.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css');?>">
  <link type="text/css" href="<?php echo base_url('assets/dataTables/datatables.min.css');?>" rel="stylesheet">
  <style type="text/css">
    .preloader {
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      z-index: 9999;
      background-color: #fff;
    }
    .preloader .loading {
      position: absolute;
      left: 50%;
      top: 50%;
      transform: translate(-50%,-50%);
      font: 14px arial;
    }
  </style>
  <!-- =======================================================
    Theme Name: Mentor
    Theme URL: https://bootstrapmade.com/mentor-free-education-bootstrap-theme/
    Author: BootstrapMade.com
    Author URL: https://bootstrapmade.com
  ======================================================= -->
</head>

<body>
  <script type="text/javascript" src="<?php echo base_url('assets/js/Chart.bundle.js');?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/js/Chart.min.js');?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/js/utils.js');?>"></script>

  <div class="preloader">
    <div class="loading">
      <img src="<?php echo base_url().'assets/img/Ellipsis.svg'?>" width="80">
      <p>Selamat Datang</p>
    </div>
  </div>
  <!--Navigation bar-->
  <nav class="navbar navbar-default navbar-fixed-top">
  

    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>

        <a class="navbar-brand" style="font-size: 15px;" href="index.php">SIMONREBEL<span> BPKAD NTB</span></a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="#banner">Home</a></li>
          <li><a href="#feature">Anggaran Keseluruhan</a></li>
          <li><a href="#organisations">Anggaran Bidang</a></li>
          <li><a href="#cta-2">Realisasi</a></li>
          <li><a href="#footer">Kontak</a></li>
          <li class="btn-trial"><a href="#" data-target="#login" data-toggle="modal">Masuk</a></li>
        </ul>
      </div>
    </div>
  </nav>
  <!--/ Navigation bar-->
  <!--Modal box-->
  <div class="modal fade" id="login" role="dialog">
    <div class="modal-dialog modal-sm">

      <!-- Modal content no 1-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center form-title">Login</h4>
        </div>
        <div class="modal-body padtrbl">

          <div class="login-box-body">
            <p class="login-box-msg">Silahkan masukkan Bidang</p>
            <div class="form-group">
              <form name="formlogin" id="formlogin" action="<?php echo base_url('C_login/aksi_login')?>" method="POST">
                <div class="form-group has-feedback">
                  
                  <input class="form-control" placeholder="Username" id="username" name="username" type="text" autocomplete="off" required />
                  <span style="display:none;font-weight:bold; position:absolute;color: red;position: absolute;padding:4px;font-size: 11px;background-color:rgba(128, 128, 128, 0.26);z-index: 17;  right: 27px; top: 5px;" id="span_loginid"></span>
                  <!---Alredy exists  ! -->
                  <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                  
                  <input class="form-control" placeholder="Password" id="password" minlength="8" name="password" type="password" autocomplete="off" required/>
                  <span style="display:none;font-weight:bold; position:absolute;color: grey;position: absolute;padding:4px;font-size: 11px;background-color:rgba(128, 128, 128, 0.26);z-index: 17;  right: 27px; top: 5px;" id="span_loginpsw"></span>
                  <!---Alredy exists  ! -->
                  <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <select class="form-control" name="user">
                  <option>Pilih Bidang</option>
                  <option>Administrator</option>
                  <option>Sekretaris</option>
                  <option>Bidang Anggaran</option>
                  <option>Bidang Akuntansi dan Pelaporan</option>
                  <option>Bidang Perbendaharaan</option>
                  <option>Bidang Pengelolaan BMD</option>
                  <option>Unit Pengelola Islamic Centre</option>
                  <option>UPTB Balai Pemanfaatan dan Pengamanan Aset</option>
                </select>
                <br>

                <input type="checkbox" onclick="myPass()"> Show Password
                  <br>
                  <br>
                  <div class="col-xs-12">
                    <button type="submit" class="btn btn-green btn-block btn-flat">Masuk</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
  <!--/ Modal box-->
  <!--Banner-->
  <section id="banner" class="section-padding">
  <div class="banner" style="width: 100%">
    <div class="bg-color">
      <div class="container">
        <div class="row">
          <div class="banner-text text-center">
            <div class="text-border">
              <h2 class="text-dec"><img src="assets/img/header.png" style="float:left; width:100% ; height:100%; size: cover;"/></h2>
            </div>
            <div class="intro-para text-center quote js-scroll-trigger">
              <p style="font-size: 24px;" class="big-text">VISI BPKAD</p>
              <p class="small-text">MENJADI INSITUSI PENGELOLA KEUANGAN DAN ASET DAERAH TERBAIK</p>
            </div>
            <a href="#feature" class="mouse-hover">
              <div class="mouse"></div>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
  </section>
  <!--/ Banner-->
  <!--Feature-->
  <section id="feature" class="section-padding">
    <div class="container">
      <div class="row">
        <div class="header-section text-center js-scroll-trigger">
          <h2>Anggaran Keseluruhan</h2>
          <hr class="bottom-line">
        </div>
        <div class="feature-info">
          <div class="fea" align="center">
            
          <div class="panel-body">   
           <table class="table table-bordered table-hover table-striped" id="data"> 
            <thead>   
              <tr>
                <th width="1%">No.</th>
                <th width="15%" align="center">Nama Kegiatan</th>
                <th width="15%" align="center">Program</th>
                <th width="10%" align="center">Bidang</th>
                <th width="10%" align="center">Pagu/Anggaran</th>
              </tr>
            </thead>  
            <tbody>
              <?php 
                $no = 1;
                foreach ($data as $d) {
              ?>
              <tr>
                <td width="1%">
                  <?php echo $no++;?> 
                </td><td width="10%">
                  <?php echo $d->nama_kegiatan;?>
                </td><td width="10%">
                  <?php echo $d->nama_program;?>
                </td><td width="5%">
                  <?php echo $d->nama_bidang;?>
                </td><td width="5%">Rp.
                  <?php echo number_format($d->pagu,2,",",".")?>
                </td>
              </tr>
             <?php } ?> 
            </tbody>

          </table>
             
             
           
          </div>
        </div> 
        </div>
      </div>
    </div>
  </section>
  <!--/ feature-->
  <!--Organisations-->
  <section id="organisations" class="section-padding">
    <div class="container">
      <div class="row">
        <div class="header-section text-center js-scroll-trigger">
          <h2>Anggaran Per Bidang</h2>
          <hr class="bottom-line">
        </div>
        <div class="feature-info">
          <div class="fea" align="center">
            
          <div class="panel-body">   
           <table class="table table-bordered table-hover table-striped" id="data1"> 
            <thead>   
              <tr>
                <th width="1%">No.</th>
                <th width="10%" align="center">Bidang</th>
                <th width="10%" align="center">Pagu/Anggaran</th>
              </tr>
            </thead>  
            <tbody>
              <?php 
                $no = 1;
                foreach ($data2 as $di) {
              ?>
              <tr>
                <td width="1%">
                  <?php echo $no++;?> 
                </td><td width="10%">
                  <?php echo $di->nama_bidang;?>
                </td><td width="10%">Rp.
                  <?php echo number_format($di->JUMLAH,2,",",".")?>
                </td>
              </tr>
             <?php } ?> 
            </tbody>

          </table>

          <!-- <?php 

          foreach ($info as $key) {
            echo $key['nama_kegiatan'] ." ". $key['nama_bidang'] ." ".   $key['pagu']/$angka[$key['id_bidang']] . "<br>";
          }
          ?> -->

          </div>
        </div> 
        </div>
      </div>
    </div>
  </section>
  <!--/ Organisations-->
  <!--Cta-->
  <section id="cta-2">
    <div class="container">
      <div class="row">
        <div class="header-section text-center js-scroll-trigger">
          <h2>Realisasi</h2>
          <hr class="bottom-line">
        </div>
        <canvas id="canvas" width="1000" height="280"></canvas>
        
      </div>
    </div>
  </section>
  <!--/ Cta-->
  <!--work-shop-->
  
  <!--/ Contact-->
  <!--Footer-->
  <section id="footer" class="footer">
    <div class="container text-center">
      <!-- End newsletter-form -->
      <ul class="social-links">
        <li><a href="http://bpkad.ntbprov.go.id/" title="bpkad.ntbprov.go.id"><i class="fa fa-chrome fa-fw"></i></a></li>
        <li><a href="https://www.facebook.com/bpkadprovntb" title="facebook.com/bpkadprovntb"><i class="fa fa-facebook fa-fw"></i></a></li>
        <li><a href="https://twitter.com/BpkadNtb" title="twitter.com/BpkadNtb"><i class="fa fa-twitter fa-fw"></i></a></li>
        <li><a href="https://www.instagram.com/ntbbpkad/" title="instagram.com/ntbbpkad/"><i class="fa fa-instagram fa-fw"></i></a></li>
        <li><a href="#" title="Phone : (0370) 627689, 625345"><i class="fa fa-phone fa-fw"></i></a></li>
        <li><a href="#" title="Fax : (0370) 627677"><i class="fa fa-fax fa-fw"></i></a></li>
      </ul>
      ©2018 BPKAD NTB. All rights reserved<br>
      Designed by <a href="http://bpkad.ntbprov.go.id/">Badan Pengelolaan Keuangan dan Aset Daerah Provinsi NTB</a>
      <div class="credits">
        <!--
          All the links in the footer should remain intact.
          You can delete the links only if you purchased the pro version.
          Licensing information: https://bootstrapmade.com/license/
          Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Mentor
        -->
        
      </div>
    </div>
  </section>
  <!--/ Footer-->
  <script>
    function myPass() {
      var x = document.getElementById("password");
      if (x.type === "password") {
        x.type = "text";
      } else {
        x.type = "password";
      }
    }
  </script>
  <script src="<?php echo base_url('bower_components/jquery/dist/jquery.min.js')?>"></script>
  <script src="<?php echo base_url('assets/dataTables/datatables.min.js')?>"></script>
  <script type="text/javascript">
      $(document).ready(function(){
        $('#data').DataTable({
          // 'paging'    : false,
          // 'info'      : false
        })
    })
  </script>
  <script type="text/javascript">
    var ctx = document.getElementById('canvas').getContext('2d');
   <?php
        foreach($data3 as $data){
            $keuangan[] = $data->JUMLAH;
            $fisik[] = $data->Fisik;
        }
    ?>
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'bar',

        // The data for our dataset
        data: {
            labels: ["Sekretaris", "Pengelolaan BMD", "Perbendaharaan", "Akuntansi dan Pelaporan", "Anggaran", "U. P. Islamic Centre", "UPT BPPA"],
            datasets: [{
                label: "Realisasi Keuangan",
                backgroundColor: 'rgb(25, 118, 210)',
                borderColor: 'rgb(25, 118, 210)', 
                data: <?php echo json_encode($keuangan);?>
                },
                { label: "Realisasi Fisik",
                backgroundColor: 'rgb(95, 207, 128)',
                borderColor: 'rgb(95, 207, 128)',
                data: <?php echo json_encode($fisik);?>
              }]

        },

        // Configuration options go here
        options: {
        scales: {
            yAxes: [{
                ticks: {
                    min: 0,
                    max: 100
                }
            }]
        }
        }
    });
    
  </script>
  <script src="<?php echo base_url('assets/js/jquery.easing.min.js')?>"></script>
  <script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
  <script src="<?php echo base_url('assets/js/custom.js')?>"></script>
  <script src="<?php echo base_url('assets/contactform/contactform.js')?>"></script>
  <script>
  $(document).ready(function(){
    $(".preloader").fadeOut();
  })
</script>

</body>

</html>
