<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Dashboard <?php echo"".$this->session->userdata('user');?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url('bower_components/bootstrap/dist/css/bootstrap.min.css');?>">
  <!-- Font Awesome -->
  
  <link rel="stylesheet" href="<?php echo base_url('bower_components/font-awesome/css/font-awesome.min.css');?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('bower_components/Ionicons/css/ionicons.min.css');?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('dist/css/AdminLTE.min.css');?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url('dist/css/skins/_all-skins.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('dist/css/sweetalert.css');?>">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <style type="text/css">
    .preloader {
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      z-index: 9999;
      background-color: #fff;
    }
    .preloader .loading {
      position: absolute;
      left: 50%;
      top: 50%;
      transform: translate(-50%,-50%);
      font: 14px arial;
    }
  </style>

</head>
<body class="hold-transition skin-blue sidebar-mini">

<div class="preloader">
  <div class="loading">
    <img src="<?php echo base_url().'assets/img/Ellipsis.svg'?>" width="80">
    <p>Harap Tunggu</p>
  </div>
</div>  
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-lg"><b>Admin</b>BPKAD</span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
                    <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown user user-menu" style="width: 160px;">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-user"></i>
              <span class="hidden-xs"><?php echo"".$this->session->userdata('user');?></span>
            </a>
            <ul class="dropdown-menu" style="width:10px;">
              <!-- User image -->

              <!-- Menu Footer-->
              <li class="user-footer">
                
                <div class="pull-right">
                  <a href="<?php echo base_url('C_Administrator/logout');?>" onclick="return confirm('Anda yakin akan keluar dari halaman ini?')" style="width:130px;" class="btn btn-default btn-flat">Log out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li>
          <a href="<?php echo base_url('C_Administrator/datauser');?>">
            <i class="fa fa-user"></i> <span>Data Pengguna</span>
          </a>
        </li>
         <li  class="active treeview menu-open">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Form Belanja</span>
          </a>
        </li>
        <li>
          <a href="<?php echo base_url('C_Administrator/databelanja');?>">
            <i class="fa fa-table"></i> <span>Data Belanja</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h4 style="text-align: center;">
        Data Belanja Langsung Badan Pengelolaan <br>Keuangan dan Aset Daerah NTB
      </h4>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-3">
        </div>

        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Data</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="<?php echo base_url('C_tambahdata');?>" method="POST">
              <div class="box-body">
                <div class="form-group">
                  <label>Bidang/Unit</label>
                  <select name="kategori" id="kategori" class="form-control" placeholder="Bidang">
                    <option value="0">-PILIH-</option>
                      <?php foreach($data->result() as $row):?>
                        <option value="<?php echo $row->id_bidang;?>"><?php echo $row->nama_bidang;?></option>
                      <?php endforeach;?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Program</label>
                  <input type="text" class="form-control" id="program" name="program" placeholder="Program">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Kegiatan</label>
                  <input type="text" class="form-control" id="kegiatan" name="kegiatan" placeholder="Kegiatan">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Nama Pejabat Pelaksana Teknis Kegiatan</label>
                  <label>Bidang/Unit</label>
                    <select name="pptk" class="pptk form-control">
                      <option value="0">-PILIH-</option>
                    </select>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->

          <!-- /.box -->

                   <!-- /.box -->

          <!-- Input addon -->

          <!-- /.box -->

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
          <!-- /.box -->
          <!-- general form elements disabled -->
          
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Designed by <a href="http://bpkad.ntbprov.go.id/"></a></b> Badan Pengelolaan Keuangan dan Aset Daerah Provinsi NTB
    </div>
    <strong>©2018<a href="http://bpkad.ntbprov.go.id/"> BPKAD NTB</a>.</strong> All rights
    reserved.
  </footer>

  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery-3.3.1.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/js/bootstrap.js'?>"></script>

<script src="<?php echo base_url('bower_components/jquery/dist/jquery.min.js')?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('bower_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('bower_components/fastclick/lib/fastclick.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('dist/js/adminlte.min.js')?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('dist/js/demo.js')?>"></script>
<script src="<?php echo base_url('dist/js/sweetalert.js')?>"></script>
<script src="<?php echo base_url('dist/js/sweet-alert.min.js')?>"></script>
<script type="text/javascript">
  function 
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#kategori').change(function(){
            var id=$(this).val();
            $.ajax({
                url : "<?php echo base_url();?>index.php/C_Administrator/get_subkategori",
                method : "POST",
                data : {id: id},
                async : false,
                dataType : 'json',
                success: function(data){
                    var html = '';
                    var i;
                    for(i=0; i<data.length; i++){
                        html += "<option value='"+data[i].id_pptk+"'>"+data[i].jabatan+"</option>";
                    }
                    $('.pptk').html(html);
                     
                }
            });
        });
    });
</script>
<script>
  $(document).ready(function(){
    $(".preloader").fadeOut();
  })
</script>
</body>
</html>
