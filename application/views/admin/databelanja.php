<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Dashboard  <?php echo"".$this->session->userdata('user');?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url('bower_components/bootstrap/dist/css/bootstrap.min.css');?>">
  <!-- Font Awesome -->
  
  <link rel="stylesheet" href="<?php echo base_url('bower_components/font-awesome/css/font-awesome.min.css');?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('bower_components/Ionicons/css/ionicons.min.css');?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('dist/css/AdminLTE.min.css');?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url('dist/css/skins/_all-skins.min.css');?>">
  <link href="<?php echo base_url('assets/dataTables/datatables.min.css')?>" rel="stylesheet">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-lg"><b>Admin</b>BPKAD</span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
                    <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown user user-menu" style="width: 160px;">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-user"></i>
              <span class="hidden-xs"><?php echo"".$this->session->userdata('user');?></span>
            </a>
            <ul class="dropdown-menu" style="width:10px;">
              <!-- User image -->

              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-center">
                  <a href="<?php echo base_url('C_Administrator/logout');?>" onclick="return confirm('Anda yakin akan keluar dari halaman ini?')" style="width:130px;" class="btn btn-default btn-flat">Log out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li>
          <a href="<?php echo base_url('C_Administrator/datauser');?>">
            <i class="fa fa-user"></i> <span>Data Pengguna</span>
          </a>
        </li>
        <li>
          <a href="<?php echo base_url('C_Administrator');?>">
            <i class="fa fa-edit"></i> <span>Form Belanja</span>
          </a>
        </li>
        <li class="active treeview menu-open">
          <a href="#">
            <i class="fa fa-table"></i> <span>Data Belanja</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h4 style="text-align: center;">
        Tabel Belanja Langsung Badan Pengelolaan Keuangan dan Aset Daerah NTB
      </h4>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
       
        <div class="panel-body">
          <table class="table table-bordered table-hover table-striped" id="dataTabel"> 
            <thead>   
              <tr>
                <th width="2%">No.</th>
                <th width="23%" align="center">Nama Kegiatan</th>
                <th width="23%">Program</th>
                <th width="10%">Bidang</th>
                <th width="10%">Nama PPTK</th>
                <th width="10%">Jabatan PPTK</th>
                <th width="8%">Aksi</th>
              </tr>
            </thead>  
            <tbody>
              <?php 
                $no = 1;
                foreach ($datax as $d) {
              ?>
              <tr>
                <td width="2%">
                  <?php echo $no++;?>
                </td><td width="23%">
                  <?php echo $d->nama_kegiatan;?>
                </td><td width="23%">
                  <?php echo $d->nama_program;?>
                </td><td width="10%">
                  <?php echo $d->nama_bidang;?>
                </td><td width="10%">
                  <?php echo $d->nama_pptk;?>
                </td><td width="10%">
                  <?php echo $d->jabatan;?>
                </td>
                <!-- <td width="10%">
                  <?php echo $dpkm_aib->nama_dosen;?>
                </td> -->
                <td width="8%">
                  <button onclick="myKegiatan(<?php echo $d->id_kegiatan;?>,<?php echo $no-1;?>,<?php echo $d->id_program;?>)"  data-toggle="modal" data-target="#modal1" class="btn btn-sm btn-info" title="Edit Data">
                        <span class="glyphicon glyphicon-pencil"></span>
                  </button>
                  <a href="<?php echo base_url().'C_hapus/hapusdata/'.$d->id_kegiatan; ?>" onclick="return confirm('Anda yakin akan menghapus data ini?')" class="btn btn-sm btn-warning" title="Hapus">
                      <span class="glyphicon glyphicon-trash"></span>
                    </a>
                  
                </td>
              </tr>
             <?php } ?> 
            </tbody>

          </table>
          <a href="<?php echo base_url().'C_Administrator/printadmin/' ?>" class="btn btn-sm btn-warning" title="Print Berkas Excel">
              <span class="glyphicon glyphicon-print"></span> Print Data Excel
          </a>
        </div>
      </div>
    </div>
  </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Designed by <a href="http://bpkad.ntbprov.go.id/"></a></b> Badan Pengelolaan Keuangan dan Aset Daerah Provinsi NTB
    </div>
    <strong>©2018<a href="http://bpkad.ntbprov.go.id/"> BPKAD NTB</a>.</strong> All rights
    reserved.
  </footer>

  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content modal-popup" >
        
        <div class='modal-header'><h3 class='white'>Edit Data</h3></div>
          <form role='form' action="<?php echo base_url('C_tambahdata/edit');?>" method="POST">
            <div class='box-body'>
              <div class='form-group'>
                  <label form='exampleInputPassword1'>No</label>
                  <input type='text' class='form-control' id='nomor' disabled>
                  <input type="hidden" name="id_program" id="idprogram">
                  <input type="hidden" name="id_kegiatan" id="idkegiatan">
                </div>
              <div class='form-group'>
                <label>Bidang/Unit</label>
                  <select name='kategori' id='kegiatan' class='form-control' placeholder='Bidang'>
                  <!-- <div id="kegiatan"></div> -->
                </select>
                </div>
                <div class='form-group'>
                  <label for='exampleInputPassword1'>Program</label>
                 
                  <input type='text' class='form-control' id='program' name='program' placeholder='Program'>
                 
                </div>
                <div class='form-group'>
                  <label for='exampleInputPassword1'>Kegiatan</label>
                  <input type='text' class='form-control' id='kegiatan' name='kegiatan' placeholder='Kegiatan'>
                 
                </div>
                <div class='form-group'>
                  <label for='exampleInputPassword1'>Nama Pejabat Pelaksana Teknis Kegiatan</label>
                    <select name='pptk' class='pptk form-control'>
                      <option value='0'>-PILIH-</option>
                    </select>
                </div>
              </div>
              <!-- /.box-body -->

              <div class='box-footer'>
                <button type='submit' class='btn btn-primary'>Submit</button>
              </div>
            </form>
        
      </div>
    </div>
</div>
<!-- jQuery 3 -->
<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery-3.3.1.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/js/bootstrap.js'?>"></script>

<script src="<?php echo base_url('bower_components/jquery/dist/jquery.min.js')?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('bower_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('bower_components/fastclick/lib/fastclick.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('dist/js/adminlte.min.js')?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('dist/js/demo.js')?>"></script>
<script src="<?php echo base_url('assets/dataTables/datatables.min.js')?>"></script>
  <script type="text/javascript">
      $(document).ready(function(){
        $('#dataTabel').DataTable({
          // 'paging'    : false,
          // 'info'      : false
        })
    })
  </script>

 <script type="text/javascript">
      function myKegiatan($id,$no,$id_program){
          document.getElementById("nomor").value = $no;
          document.getElementById("idprogram").value = $id_program;
          document.getElementById("idkegiatan").value = $id;
          $.ajax({
                  type: 'post',
                  url: "<?php echo base_url('C_Administrator/coba');?>",
                  data: {
                  id_kegiatan:$id,
                  no:$no
                  },
                  success: function (response) {
                  $( '#kegiatan' ).html(response);
                  }
                 });
      }
  </script> 
  <script type="text/javascript">
    $(document).ready(function(){
        $('#kegiatan').change(function(){
            var id=$(this).val();
            $.ajax({
                url : "<?php echo base_url();?>index.php/C_Administrator/get_subkategori",
                method : "POST",
                data : {id: id},
                async : false,
                dataType : 'json',
                success: function(data){
                    var html = '';
                    var i;
                    for(i=0; i<data.length; i++){
                        html += "<option value='"+data[i].id_pptk+"'>"+data[i].jabatan+"</option>";
                    }
                    $('.pptk').html(html);
                     
                }
            });
        });
    });
</script>
</body>
</html>
