<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Dashboard <?php echo"".$this->session->userdata('user');?> </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url('bower_components/bootstrap/dist/css/bootstrap.min.css');?>">
  <!-- Font Awesome -->
  
  <link rel="stylesheet" href="<?php echo base_url('bower_components/font-awesome/css/font-awesome.min.css');?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('bower_components/Ionicons/css/ionicons.min.css');?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('dist/css/AdminLTE.min.css');?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url('dist/css/skins/_all-skins.min.css');?>">
  <link href="<?php echo base_url('assets/dataTables/datatables.min.css')?>" rel="stylesheet">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <style type="text/css">
    .preloader {
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      z-index: 9999;
      background-color: #fff;
    }
    .preloader .loading {
      position: absolute;
      left: 50%;
      top: 50%;
      transform: translate(-50%,-50%);
      font: 14px arial;
    }
  </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="preloader">
  <div class="loading">
    <img src="<?php echo base_url().'assets/img/Ellipsis.svg'?>" width="80">
    <p>Harap Tunggu</p>
  </div>
</div> 
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-lg"><b>Admin</b>BPKAD</span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
                    <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown user user-menu" style="width: 160px;">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="text-align: center;">
              <i class="fa fa-user"></i>
              <span class="hidden-xs"><i class="fa fa-user"></i><?php echo"".$this->session->userdata('user');?></span>
            </a>
            <ul class="dropdown-menu" style="width:10px;">
              <!-- User image -->

              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-center">
                  <a href="<?php echo  base_url('C_Administrator/logout');?>" onclick="return confirm('Anda yakin akan keluar dari halaman ini?')" style="width:130px;" class="btn btn-default btn-flat">Log out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active treeview menu-open">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Form Belanja</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="text-align: center; font-size: 18px">
      <h4>
        Data Belanja Langsung</h4><?php echo"".$this->session->userdata('user');?>
      <h4>Badan Pengelolaan Keuangan dan Aset Daerah NTB
      </h4>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
       
        <div class="panel-body">
          <table class="table table-bordered table-hover table-striped" id="dataTabel"> 
            <thead>   
              <tr>
                <th width="1%">No.</th>
                <th width="15%" style="text-align: center;">Nama Kegiatan</th>
                <th width="15%" style="text-align: center;">Program</th>
                <th width="10%" style="text-align: center;">PPTK</th>
                <th width="10%" style="text-align: center;">Pagu/Anggaran</th>
                <th width="10%" style="text-align: center;">Renc. Kas Tri I</th>
                <th width="10%" style="text-align: center;">Renc. Kas Tri II</th>
                <th width="10%" style="text-align: center;">Renc. Kas Tri III</th>
                <th width="10%" style="text-align: center;">Renc. Kas Tri IV</th>
                <th width="10%" style="text-align: center;">Real. Keuangan</th>
                <th width="10%" style="text-align: center;">Real. Fisik</th>
                <th width="8%" style="text-align: center;">Aksi</th>
              </tr>
            </thead>  
            <tbody>
              <?php 
                $no = 1;
                foreach ($databdg as $d) {
              ?>
              <tr>
                <td width="1%">
                  <?php echo $no++;?> 
                </td><td width="10%">
                  <?php echo $d->nama_kegiatan;?>
                </td><td width="10%">
                  <?php echo $d->nama_program;?>
                </td><td width="8%">
                  <?php echo $d->nama_pptk;?>
                </td><td width="8%">
                  <?php echo number_format($d->pagu,2,",",".")?>
                </td><td width="10%">
                  <?php echo number_format($d->tri1,2,",",".")?>
                </td><td width="10%">
                  <?php echo number_format($d->tri2,2,",",".")?>
                </td><td width="10%">
                  <?php echo number_format($d->tri3,2,",",".")?>
                </td><td width="10%">
                  <?php echo number_format($d->tri4,2,",",".")?>
                </td>
                <td width="10%">
                  <?php echo number_format($d->realkeu,2,",",".")?>
                </td>
                <td width="10%">
                  <?php echo $d->realfisik;?> %
                </td>
                <td width="5%">
                  <button onclick="myKegiatan('<?php echo $d->id_kegiatan;?>','<?php echo $d->id_program;?>', '<?php echo $d->nama_program;?>','<?php echo $d->nama_kegiatan;?>',<?php echo $d->pagu;?>,<?php echo $d->tri1;?>,<?php echo $d->tri2;?>,<?php echo $d->tri3;?>,<?php echo $d->tri4;?>,<?php echo $d->realkeu;?>,<?php echo $d->realfisik;?>)" data-toggle="modal" data-target="#modal1" class="btn btn-sm btn-info" title="Edit Data">
                        <span class="glyphicon glyphicon-pencil"></span>
                  </button>
                  <a href="<?php echo base_url().'C_hapus/hapusdatabdg/'.$d->id_kegiatan; ?>"  onclick="return confirm('Anda yakin akan menghapus data ini?')" class="warning btn btn-sm btn-warning" title="Hapus">
                      <span class="glyphicon glyphicon-trash"></span>
                  </a>
                  
                </td>
              </tr>
             <?php } ?> 
            </tbody>

          </table>
          <a href="<?php echo base_url().'C_Administrator/printbdg/' ?>" class="btn btn-sm btn-success" title="Print Berkas Excel">
              <span class="glyphicon glyphicon-print"></span> Print Data Excel
          </a>
        </div>
      </div>
    </div>
  </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Designed by <a href="http://bpkad.ntbprov.go.id/"></a></b> Badan Pengelolaan Keuangan dan Aset Daerah Provinsi NTB
    </div>
    <strong>©2018<a href="http://bpkad.ntbprov.go.id/"> BPKAD NTB</a>.</strong> All rights
    reserved.
  </footer>

  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content modal-popup" >
        
        <div class='modal-header'><h3 class='white'>Edit Data</h3></div>
          <form role='form' action="<?php echo base_url('C_tambahdata/editbidang');?>" method="POST">
            <div class='box-body'>
              <div class='form-group'>
                  <label form='exampleInputPassword1'>Nama Kegiatan</label>
                  <input type='text' class='form-control' id='namkeg' disabled>
                  <label form='exampleInputPassword1'>Nama Program</label>
                  <input type='text' class='form-control' id='namprog' disabled>
                  <input type="hidden" name="id_program" id="idprogram">
                  <input type="hidden" name="id_kegiatan" id="idkegiatan">
                </div>
              <div class='form-group'>
                  <label for='exampleInputPassword1'>Pagu</label>
                  <input type='text' class='form-control' id='pagu' name='pagu' placeholder='Pagu/Anggaran(dalam Rupiah)'       onchange="validasi()">
              </div>
              <div class='form-group'>
                  <label for='exampleInputPassword1'>Rencana Kas Tri I</label>
                  <input type='text' class='form-control' id='kas1' name='kas1' placeholder='Rencana Kas Tri I(dalam Rupiah)' onchange="validasi()">
              </div>
              <div class='form-group'>
                  <label for='exampleInputPassword1'>Rencana Kas Tri II</label>
                  <input type='text' class='form-control' id='kas2' name='kas2' placeholder='Rencana Kas Tri II(dalam Rupiah)' onchange="validasi()">
              </div>
              <div class='form-group'>
                  <label for='exampleInputPassword1'>Rencana Kas Tri III</label>
                  <input type='text' class='form-control' id='kas3' name='kas3' placeholder='Rencana Kas Tri III(dalam Rupiah)' onchange="validasi()">
              </div>
              <div class='form-group'>
                  <label for='exampleInputPassword1'>Rencana Kas Tri IV</label>
                  <input type='text' class='form-control' id='kas4' name='kas4' placeholder='Rencana Kas Tri IV(dalam Rupiah)'  onchange="validasi()">
              </div>
              <div class='form-group'>
                  <label for='exampleInputPassword1'>Realisasi Keuangan</label>
                  <input type='text' class='form-control' id='realk' name='realk' placeholder='Realisasi Keuangan(dalam Rupiah)'  onchange="validasi()">  
              </div>
              <div class='form-group'>
                  <label for='exampleInputPassword1'>Realisasi Fisik</label>
                  <input type='text' class='form-control' id='realfisik' name='realfisik' placeholder='Realisasi Fisik(dalam %)'>   
              </div>
            </div>
          <div class='box-footer'>
            <button type='submit' class='btn btn-primary'>Submit</button>
          </div>
        </form>      
      </div>
    </div>
</div>
<!-- jQuery 3 -->
<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery-3.3.1.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/js/bootstrap.js'?>"></script>

<script src="<?php echo base_url('bower_components/jquery/dist/jquery.min.js')?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('bower_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('bower_components/fastclick/lib/fastclick.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('dist/js/adminlte.min.js')?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('dist/js/demo.js')?>"></script>
<script src="<?php echo base_url('assets/dataTables/datatables.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/jquery.maskMoney.js')?>"></script>
  <script type="text/javascript">
      $(document).ready(function(){
        $('#dataTabel').DataTable({
          // 'paging'    : false,
          // 'info'      : false
          'ordering'     : false
        })
    })
  </script>
  
  <script type="text/javascript">
    function validasi(){
      var pagu = parseInt(document.getElementById('pagu').value);
      var kas1 = parseInt(document.getElementById('kas1').value);
      var kas2 = parseInt(document.getElementById('kas2').value);
      var kas3 = parseInt(document.getElementById('kas3').value);
      var kas4 = parseInt(document.getElementById('kas4').value);
      var realk = parseInt(document.getElementById('realk').value);
      var total = kas1+kas2+kas3+kas4;


      if (total>pagu||kas1>pagu||kas2>pagu||kas3>pagu||kas4>pagu){
        alert ("Total Nilai Anggaran Triwulan melebihi Pagu Keseluruhan")
      }
      if (realk>pagu){
        alert ("Total Nilai Realisasi Keuangan melebihi Pagu")
      }
      

    }
  </script>

  
  <script type="text/javascript">
      function myKegiatan($id,$id_program,$namkeg,$namprog,$pagu,$kas1,$kas2,$kas3,$kas4,$realkeu,$realfisik){
          document.getElementById("idprogram").value = $id_program;
          document.getElementById("idkegiatan").value = $id;
          document.getElementById("namkeg").value = $namkeg;
          document.getElementById("namprog").value = $namprog;
          document.getElementById("pagu").value = $pagu;
          document.getElementById("kas1").value = $kas1;
          document.getElementById("kas2").value = $kas2;
          document.getElementById("kas3").value = $kas3;
          document.getElementById("kas4").value = $kas4;
          document.getElementById("realk").value = $realkeu;
          document.getElementById("realfisik").value = $realfisik;
      }
  </script> 
  <script>
  $(document).ready(function(){
    $(".preloader").fadeOut();
  })
</script>
  </body>
</html>
