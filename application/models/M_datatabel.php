<?php
	class M_datatabel extends CI_Model{
		 function get_data(){
		  	$hasil=$this->db->query("SELECT * FROM kegiatan, bidang, program, pptk WHERE kegiatan.id_bidang = bidang.id_bidang AND kegiatan.id_pptk = pptk.id_pptk AND kegiatan.id_program = program.id_program");
	        return $hasil->result();
		 }

		 function get_data_bidang($bidang){
		 	$sql = "SELECT id_bidang FROM bidang where nama_bidang LIKE '%".$bidang."%'";
          	$query = $this->db->query($sql);
            foreach ($query->result_array() as $row) {
               $id_bidang=$row['id_bidang'];
            }
		 	$hasil=$this->db->query("SELECT DISTINCT program.id_program, program.nama_program, kegiatan.id_kegiatan, kegiatan.nama_kegiatan, pptk.nama_pptk, kegiatan.pagu, kegiatan.tri1, kegiatan.tri2, kegiatan.tri3, kegiatan.tri4, kegiatan.realkeu, kegiatan.realfisik FROM kegiatan, bidang, program, pptk WHERE kegiatan.id_bidang =".$id_bidang."  AND kegiatan.id_pptk = pptk.id_pptk AND kegiatan.id_program = program.id_program");
	        return $hasil->result();
		 }
	
		 function get_data_user(){
		 	$hasil=$this->db->query("SELECT * FROM pengguna");
		 	return $hasil->result();
		 }
		 
		 function tabel1(){
		 	$hasil=$this->db->query("SELECT nama_kegiatan, nama_program, pagu, nama_bidang from kegiatan, program, bidang where kegiatan.id_program=program.id_program and kegiatan.id_bidang=bidang.id_bidang ORDER BY bidang.id_bidang ASC");
	        return $hasil->result();
		 }

		 function tabel2(){
		 	$hasil=$this->db->query("SELECT nama_bidang, SUM(kegiatan.pagu) as JUMLAH from kegiatan LEFT JOIN bidang on kegiatan.id_bidang=bidang.id_bidang group by bidang.id_bidang");
	        return $hasil->result();
		 }
		 function chart1(){
		 	$hasil=$this->db->query("SELECT nama_bidang, SUM(kegiatan.realkeu)/SUM(kegiatan.pagu)*100 as JUMLAH, SUM(kegiatan.realfisik)/COUNT(kegiatan.nama_kegiatan) as Fisik from kegiatan LEFT JOIN bidang on kegiatan.id_bidang=bidang.id_bidang group by bidang.id_bidang DESC");
		 	return $hasil->result();
		 }
	}

?>