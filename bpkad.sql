-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 04 Jan 2019 pada 01.56
-- Versi Server: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bpkad`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `bidang`
--

CREATE TABLE `bidang` (
  `id_bidang` int(3) NOT NULL,
  `nama_bidang` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bidang`
--

INSERT INTO `bidang` (`id_bidang`, `nama_bidang`) VALUES
(1, 'Bidang Anggaran'),
(2, 'Bidang Akuntansi dan Pelaporan'),
(3, 'Bidang Perbendaharaan'),
(4, 'Bidang Pengelolaan BMD'),
(5, 'Unit Pengelola Islamic Centre'),
(6, ' UPTB Balai Pemanfaatan dan Pengamanan Aset'),
(7, 'Sekretaris');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kegiatan`
--

CREATE TABLE `kegiatan` (
  `id_kegiatan` int(3) NOT NULL,
  `nama_kegiatan` varchar(100) NOT NULL,
  `id_bidang` int(3) NOT NULL,
  `id_pptk` int(3) NOT NULL,
  `id_program` int(3) NOT NULL,
  `pagu` bigint(15) NOT NULL,
  `tri1` bigint(15) NOT NULL,
  `tri2` bigint(15) NOT NULL,
  `tri3` bigint(15) NOT NULL,
  `tri4` bigint(15) NOT NULL,
  `realkeu` bigint(15) NOT NULL,
  `realfisik` tinyint(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kegiatan`
--

INSERT INTO `kegiatan` (`id_kegiatan`, `nama_kegiatan`, `id_bidang`, `id_pptk`, `id_program`, `pagu`, `tri1`, `tri2`, `tri3`, `tri4`, `realkeu`, `realfisik`) VALUES
(57, 'Penyediaan jasa surat menyurat', 7, 703, 56, 28000000, 6999000, 7166000, 7166000, 6669000, 25000000, 50),
(58, 'Penyediaan jasa komunikasi; sumber daya air dan listrik', 7, 703, 56, 617600002, 167001000, 166951000, 166651002, 116997000, 555500000, 100),
(59, 'Penyediaan jasa administrasi keuangan', 7, 703, 56, 0, 0, 0, 0, 0, 0, 0),
(60, 'Penyediaan peralatan dan perlengkapan kantor', 7, 703, 56, 973260000, 368660000, 539300000, 55300000, 10000000, 900000000, 0),
(61, 'Penyediaan bahan bacaan dan peraturan perundang-undangan', 7, 703, 56, 31500000, 7875000, 7875000, 7875000, 7875000, 30000000, 0),
(62, 'Penyediaan makanan dan minuman', 7, 703, 56, 381756378, 127940000, 107940000, 107940000, 37936378, 300000000, 0),
(63, 'Penyelarasan Program Pemerintah Pusat dan Daerah', 7, 703, 56, 225000000, 76000000, 102000000, 46000000, 1000000, 220000000, 0),
(64, 'Penyediaan jasa administrasi dan teknis perkantoran', 7, 703, 56, 104300000, 26076000, 30076000, 28076000, 20072000, 104000000, 0),
(65, 'Penyelarasan Program Pemerintah Provinsi dan Kabupaten/Kota', 7, 703, 56, 125000000, 41000000, 42000000, 42000000, 0, 124000000, 0),
(66, 'Pengadaan kendaraan dinas/operasional', 4, 401, 57, 601820000, 0, 0, 601820000, 0, 600000000, 0),
(67, 'Pemeliharaan rutin/berkala gedung kantor', 7, 703, 57, 225700000, 107700000, 107500000, 6500000, 4000000, 225000000, 0),
(68, 'Pemeliharaan rutin/berkala kendaraan dinas/operasional', 7, 703, 57, 580394000, 144798000, 163198000, 163198000, 109200000, 580300000, 0),
(69, 'Pemeliharaan rutin/berkala peralatan kantor', 7, 703, 57, 199000000, 46000000, 61500000, 67500000, 24000000, 198000000, 0),
(70, 'Rehabilitasi sedang/berat rumah dinas', 4, 401, 57, 3760890000, 40000000, 3281465000, 429425000, 10000000, 3750000000, 0),
(71, 'Rehabilitasi sedang/berat gedung kantor', 4, 401, 57, 1365070000, 815070000, 550000000, 0, 0, 1350000000, 0),
(72, 'Pembinaan mental dan fisik aparatur', 7, 703, 58, 66939500, 16785000, 17285000, 16785000, 16084500, 66000000, 0),
(73, 'Peningkatan SDM Aparatur', 7, 703, 58, 125000000, 42000000, 48000000, 33000000, 2000000, 120000000, 0),
(74, 'Penyusunan laporan capaian kinerja dan ikhtisar realisasi kinerja SKPD', 7, 702, 59, 179000000, 151000000, 28000000, 0, 0, 175000000, 0),
(75, 'Penyusunan Rencana Kerja SKPD', 7, 702, 59, 273600000, 65900000, 38900000, 165900000, 2900000, 273000000, 0),
(76, 'Peningkatan Manajemen Aset/Barang Milik Daerah', 4, 401, 60, 746500000, 139300000, 233950000, 223950000, 149300000, 740000000, 0),
(77, 'Penyusunan Peraturan Daerah dan Peraturan Gubernur tentang APBD', 1, 101, 61, 5827205000, 1441525000, 1596902500, 1435702500, 1353075000, 5000000000, 0),
(78, 'Penyusunan Peraturan Daerah dan Peraturan Gubernur tentang Perubahan APBD', 1, 101, 61, 529450000, 83750000, 220975000, 199725000, 25000000, 529000000, 0),
(79, 'Penyusunan rancangan peraturan daerah dan peraturan KDH tentang', 2, 201, 61, 386000000, 25000000, 203000000, 115500000, 42500000, 380000000, 0),
(80, 'Sosialisasi Paket Regulasi tentang Pengelolaa Keuangan Daerah', 7, 702, 61, 276250000, 122350000, 151400000, 2500000, 0, 276000000, 0),
(82, 'Pemindahtanganan dan Penghapusan Barang Milik Daerah', 4, 401, 61, 607270000, 121454000, 182381000, 181546000, 121889000, 600000000, 0),
(83, 'Revaluasi/appraisal aset/barang daerah', 4, 401, 61, 104200000, 20840000, 31260000, 31000000, 21100000, 100000000, 0),
(84, 'Peningkatan Pelayanan Kas Daerah/Kuasa BUD', 3, 304, 61, 736007000, 227425000, 201557060, 186425000, 120599940, 736000000, 0),
(85, 'Evaluasi dan Monitoring Pelaksanaan Hibah dan Bantuan Sosial', 7, 702, 61, 125000000, 52000000, 10000000, 60000000, 3000000, 124000000, 0),
(86, 'Implementasi Aplikasi Sistem Informasi Manajemen Pengelolaan Keuangan Daerah', 7, 702, 61, 187600000, 55200000, 67000000, 32700000, 32700000, 187000000, 0),
(87, 'Asistensi dan Pembahasan RKA/RKAP/DPA/DPPA-SKPD', 1, 101, 61, 161875000, 15000000, 107500000, 39375000, 0, 161800000, 0),
(88, 'Evaluasi dan Pengendalian Bantuan Keuangan Provinsi', 7, 702, 61, 42240000, 10000000, 32240000, 0, 0, 42000000, 0),
(89, 'Pelatihan Pengelolaan Keuangan Daerah', 7, 702, 61, 59500000, 15000000, 44500000, 0, 0, 59000000, 0),
(90, 'Penyusunan Laporan Kinerja Keuangan Kab/Kota', 2, 201, 61, 72500000, 0, 0, 72500000, 0, 70000000, 0),
(91, 'Penyusunan Regulasi Pengelolaan Keuangan Daerah', 7, 702, 61, 197250000, 85750000, 102500000, 7000000, 2000000, 197000000, 0),
(92, 'Penyusunan Dokumen Informasi Keuangan Daerah', 3, 304, 61, 356140000, 237140000, 109000000, 10000000, 0, 356000000, 0),
(93, 'Penataan dan Pengembangan Informasi Arsip Keuangan', 7, 703, 61, 345827750, 94251000, 109078750, 118751000, 23747000, 345000000, 0),
(94, 'Peningkatan Kapasitas Pengelolaan Keuangan Daerah', 1, 101, 61, 1014250000, 227064000, 277564000, 299064000, 210558000, 1014000000, 0),
(95, 'Pengelolaan Administrasi Keuangan PPKD', 7, 702, 61, 8273650000, 1511850000, 2464600000, 2270975000, 2026225000, 8270000000, 0),
(96, 'Publikasi Informasi Keuangan Daerah', 7, 702, 61, 2072000000, 820249000, 838517000, 222117000, 191117000, 2070000000, 0),
(97, 'Penyusunan Laporan Keuangan Pemerintah Daerah', 2, 201, 61, 640560000, 289975000, 244975000, 58975000, 46635000, 640000000, 0),
(98, 'Rekonsiliasi Realisasi Anggaran Pendapatan dan Belanja Pemerintah Provinsi NTB', 2, 201, 61, 226782000, 44450000, 46950000, 68850000, 66532000, 225000000, 0),
(99, 'Pembinaan Penatausahaan Keuangan Daerah', 3, 304, 61, 177400000, 114900000, 20000000, 42500000, 0, 175000000, 0),
(100, 'Evaluasi rancangan peraturan daerah tentang APBD Kabupaten/Kota', 1, 101, 62, 651300900, 80000000, 180500000, 267800900, 123000000, 651000000, 0),
(101, 'Evaluasi Pertanggungjawaban Pelaksanaan APBD Kab/Kota', 2, 201, 62, 319897450, 0, 15000000, 288897450, 16000000, 310000000, 0),
(102, 'Asistensi Pengelolaan Keuangan Daerah Kabupaten/Kota', 1, 101, 62, 76858500, 5000000, 71858500, 0, 0, 75000000, 0),
(103, 'Penyelarasan Program Tim Anggaran Pemerintah Daerah Kab/Kota', 1, 101, 62, 397379500, 169448000, 86648000, 91127500, 50156000, 395000000, 0),
(104, 'Tuntutan Perbendaharaan dan Tuntutan Ganti Rugi  (TP-TGR)', 2, 201, 63, 329619700, 76775000, 76900000, 97900000, 78044700, 325000000, 0),
(105, 'Penyediaan jasa surat menyurat', 6, 601, 56, 0, 0, 0, 0, 0, 0, 0),
(106, 'Penyediaan jasa komunikasi; sumber daya air dan listrik', 6, 601, 56, 0, 0, 0, 0, 0, 0, 0),
(107, 'Penyediaan jasa administrasi keuangan', 6, 601, 56, 0, 0, 0, 0, 0, 0, 0),
(108, 'Penyediaan peralatan dan perlengkapan kantor', 6, 601, 56, 0, 0, 0, 0, 0, 0, 0),
(109, 'Penyediaan bahan bacaan dan peraturan perundang-undangan', 6, 601, 56, 0, 0, 0, 0, 0, 0, 0),
(110, 'Penyediaan makanan dan minuman', 6, 601, 56, 0, 0, 0, 0, 0, 0, 0),
(111, 'Penyelarasan Program Pemerintah Pusat dan Daerah', 6, 601, 56, 0, 0, 0, 0, 0, 0, 0),
(112, 'Penyelarasan Program Pemerintah Provinsi dan Kabupaten/Kota', 6, 601, 56, 0, 0, 0, 0, 0, 0, 0),
(113, 'Pemeliharaan rutin/berkala gedung kantor', 6, 601, 57, 0, 0, 0, 0, 0, 0, 0),
(114, 'Pemeliharaan rutin/berkala kendaraan dinas/operasional', 6, 601, 57, 0, 0, 0, 0, 0, 0, 0),
(115, 'Pemeliharaan rutin/berkala peralatan kantor', 6, 601, 57, 0, 0, 0, 0, 0, 0, 0),
(116, 'Peningkatan SDM Aparatur', 6, 601, 58, 0, 0, 0, 0, 0, 0, 0),
(117, 'Penyusunan laporan capaian kinerja dan ikhtisar realisasi kinerja SKPD', 6, 601, 59, 0, 0, 0, 0, 0, 0, 0),
(118, 'Penyusunan Rencana Kerja SKPD', 6, 601, 59, 0, 0, 0, 0, 0, 0, 0),
(119, 'Optimalisasi Pemanfaatan Barang Milik Daerah', 6, 601, 61, 0, 0, 0, 0, 0, 0, 0),
(120, 'Sertifikasi tanah', 6, 601, 61, 0, 0, 0, 0, 0, 0, 0),
(121, 'Pengamanan dan Penertiban Aset', 6, 601, 61, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengguna`
--

CREATE TABLE `pengguna` (
  `id_pengguna` int(3) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `nama_bidang` varchar(50) DEFAULT NULL,
  `lvl_user` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengguna`
--

INSERT INTO `pengguna` (`id_pengguna`, `username`, `password`, `nama_bidang`, `lvl_user`) VALUES
(1, 'admin', 'adminbpkad', 'Administrator', 1),
(2, 'sekretaris', 'sekretaris', 'Sekretaris', 2),
(3, 'bidanggar', 'bidanggar', 'Bidang Anggaran', 2),
(4, 'bidakpel', 'bidakpel', 'Bidang Akuntansi dan Pelaporan', 2),
(5, 'bidbenda', 'bidbenda', 'Bidang Perbendaharaan', 2),
(6, 'bidpbmd', 'bidpbmd', 'Bidang Pengelolaan BMD', 2),
(7, 'unitpic', 'unitpisce', 'Unit Pengelola Islamic Centre', 2),
(8, 'uptbalai', 'uptbalai', 'UPTB Balai Pemanfaatan dan Pengamanan Aset', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pptk`
--

CREATE TABLE `pptk` (
  `id_pptk` int(5) NOT NULL,
  `nama_pptk` varchar(50) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `id_bidang` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pptk`
--

INSERT INTO `pptk` (`id_pptk`, `nama_pptk`, `jabatan`, `id_bidang`) VALUES
(101, 'Nursalim, S.Sos, MM', 'Kepala Bidang Anggaran', 1),
(102, 'Muhammad Fauzi, SE', 'Kasubbid Anggaran I', 1),
(103, 'Yul Hardiansyah, SE, MM', 'Kasubbid Anggaran II', 1),
(104, 'Siti Ramlah, SE', 'Kasubbid Bina & Evaluasi Anggaran Kabupaten/Kota', 1),
(201, 'Drs. H. Azhar, MM', 'Kepala Bidang Akuntansi & Pelaporan', 2),
(202, 'Desta Damuharti, SE, M.Ak', 'Kasubbid Akuntansi I', 2),
(203, 'Indra Wahyuni, SE, M.Ak', 'Kasubbid Akuntansi II', 2),
(204, 'Ayu Muliati, SE, MM', 'Kasubbid Pelaporan & Evaluasi Keuangan Daerah', 2),
(301, 'Drs. Syamsuddar, M.Ak', 'Kepala Bidang Perbendaharaan', 3),
(302, 'M. Rusli, SE', 'Kasubbid Pengelolaan KAS', 3),
(303, 'Zuharia Mardiyanti, SE', 'Kasubbid Perbendaharaan I', 3),
(304, 'Lale Ria Aryani, SE', 'Kasubbid Perbendaharaan II', 3),
(401, 'Najib, SH', 'Kepala Bidang Pengelolaan BMD', 4),
(402, 'Ismail, S.Sos, MM', 'Kasubbid Perencanaan Kebutuhan & Pengadaan BMD', 4),
(403, 'Diah Ayu Shinta Wati, S.STP', 'Kasubbid Pemeliharaan & Penghapusan BMD', 4),
(404, 'Husnul Pikri, S.Sos', 'Kasubbid Penatausahaan dan Pembinaan Aset', 4),
(501, 'Sulaiman Jamsuri, S.Ag, M.AP', 'Kepala Unit Pengelola Islamic Centre', 5),
(502, 'Rizqi Hermawan, SE', 'Kasubbag Tata Usaha', 5),
(503, 'Arjunawan Mardjun, S.STP', 'Kasi Pemeliharaan Sarana & Prasarana', 5),
(504, 'Gustini Widijaningsih, S.Pd', 'Kasi Pemanfaatan, Pengembangan Usaha & Bisnis', 5),
(601, 'Drs. H. Muhammad Anwar', 'Kepala UPTB Balai Pemanfaatan dan Pengamanan Aset', 6),
(602, 'Ulul Azmi, SE', 'Kasubbad Tata Usaha', 6),
(603, 'IGA. Diena Riarti, U.P.ST', 'Kasi Pemeliharaan Aset', 6),
(604, 'Lalu Akhdiyat Aliyansyah, SE', 'Kasi Pengamanan Aset', 6),
(701, 'Drs. H. Zainul Islam', 'Sekretaris', 7),
(702, 'Lalu Miftahul Ulum, ST', 'Kasubbag Program', 7),
(703, 'Hanan Istiqlal, SH', 'Kasubbag Umum', 7),
(704, 'M. Syamsul Hadi, SE', 'Kasubbag Keuangan', 7);

-- --------------------------------------------------------

--
-- Struktur dari tabel `program`
--

CREATE TABLE `program` (
  `id_program` int(3) NOT NULL,
  `nama_program` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `program`
--

INSERT INTO `program` (`id_program`, `nama_program`) VALUES
(56, 'Program Pelayanan Administrasi Perkantoran'),
(57, 'Program Peningkatan Sarana dan Prasarana Aparatur'),
(58, 'Program Peningkatan Kapasitas Sumber Daya Aparatur'),
(59, 'Program Peningkatan Pengembangan Sistem Pelaporan Capaian Kinerja dan Keuangan'),
(60, 'Program Peningkatan Kapasitas Pengelolaan Keuangan Daerah'),
(61, 'Program Peningkatan dan Pengembangan Pengelolaan Keuangan Daerah'),
(62, 'Program Pembinaan dan Fasilitasi Pengelolaan Keuangan Kabupaten/Kota'),
(63, 'Program Peningkatan Sistem Pengawasan Internal dan Pengendalian Pelaksanaan Kebijakan KDH'),
(64, 'ea'),
(65, 'program itu'),
(66, 'program itu');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bidang`
--
ALTER TABLE `bidang`
  ADD PRIMARY KEY (`id_bidang`);

--
-- Indexes for table `kegiatan`
--
ALTER TABLE `kegiatan`
  ADD PRIMARY KEY (`id_kegiatan`),
  ADD KEY `id_pptk` (`id_pptk`),
  ADD KEY `id_bidang` (`id_bidang`),
  ADD KEY `id_program` (`id_program`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`id_pengguna`);

--
-- Indexes for table `pptk`
--
ALTER TABLE `pptk`
  ADD PRIMARY KEY (`id_pptk`),
  ADD KEY `id_bidang` (`id_bidang`);

--
-- Indexes for table `program`
--
ALTER TABLE `program`
  ADD PRIMARY KEY (`id_program`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bidang`
--
ALTER TABLE `bidang`
  MODIFY `id_bidang` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `kegiatan`
--
ALTER TABLE `kegiatan`
  MODIFY `id_kegiatan` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;
--
-- AUTO_INCREMENT for table `pengguna`
--
ALTER TABLE `pengguna`
  MODIFY `id_pengguna` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `program`
--
ALTER TABLE `program`
  MODIFY `id_program` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `kegiatan`
--
ALTER TABLE `kegiatan`
  ADD CONSTRAINT `kegiatan_ibfk_2` FOREIGN KEY (`id_pptk`) REFERENCES `pptk` (`id_pptk`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `kegiatan_ibfk_3` FOREIGN KEY (`id_bidang`) REFERENCES `bidang` (`id_bidang`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `kegiatan_ibfk_4` FOREIGN KEY (`id_program`) REFERENCES `program` (`id_program`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `pptk`
--
ALTER TABLE `pptk`
  ADD CONSTRAINT `pptk_ibfk_1` FOREIGN KEY (`id_bidang`) REFERENCES `bidang` (`id_bidang`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
